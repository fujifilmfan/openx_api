#!/usr/bin/env python

import unittest
# import os
from rick import ParseArgs, APIRequest


class RickTestCase(unittest.TestCase):
    # sets up a parser and Timecode instance
    @classmethod
    def setUpClass(cls):
        cls.arg_parser = ParseArgs()
        cls.parser = cls.arg_parser.create_parser()
        # cls.customers = Customers()
        # cls.api_request = APIRequest()
        # cls.api_request.request_url = 'http://some.domain.com/ox/4.0'

    @staticmethod
    def instantiate_apirequest(args):
        apirequest = APIRequest(args)
        return apirequest

    # this is overkill for simple tests, but these paths can be combined with
    # predefined parameters to test full URL construction
    @staticmethod
    def get_paths():
        paths = {
            'object': 'object',
            'available_fields': 'object/available_fields',
            'get_report_columns': 'object/get_report_columns',
            'get_report_inputs': 'object/get_report_inputs',
            'get_reportlist': 'object/get_reportlist',
            'id': 'object/123456789',
            'run': 'object/run',
            'segment': 'object/segment',
            'segments': 'object/segment1/segment2',
            'segments_out_of_order': 'object/segment2/segment1',
            'available_fields_segments': 'object/available_fields/segment1/segment2'
        }
        return paths

    @staticmethod
    def get_params(*args):
        options = {
            'null': {},
            'l10': {'limit': 10},
            'l100': {'limit': 100},
            'o0': {'offset': 0},
            'o10': {'offset': 10},
            'get_report_columns': {'report': 'report_type'},
            'get_report_inputs': {'report': 'report_type'},
            'report': {'report': 'report_type'},
            'default_start_date': {'start_date': '1'},
            'relative_start_date': {'start_date': '2'},
            'absolute_start_date': {'start_date': 'YYYY-mm-dd HH:MM:SS'},
            'default_end_date': {'end_date': '1'},
            'relative_end_date': {'end_date': '2'},
            'absolute_end_date': {'end_date': 'YYYY-mm-dd HH:MM:SS'},
            'default_report_format': {'report_format': 'json'},
            'report_format': {'report_format': 'format'},
            'breakout': {'do_break': 'breakout'},
            'breakouts': {'do_break': 'breakout1,breakout2'},
            'type_full': {'type_full': 'object.some_type'},
            'param': {'param': 'test'},
            'params': {'param1': 'test1', 'param2': 'test2'}
        }
        # params = options[args[0]].copy()
        params = {}
        for arg in args:
            params.update(options[arg])
        return params

    # -----Test the construct_path() method - basic functionality-----

    # minimum required arguments
    def test_construct_path_w_instance_w_object(self):
        args = self.parser.parse_args(['instance', 'object'])
        api_request = self.instantiate_apirequest(args)
        path = self.get_paths()['object']
        self.assertEqual(api_request.construct_path(), path)

    # changing the order of required arguments should result in a failure
    def test_construct_path_w_instance_w_object_mispositioned(self):
        args = self.parser.parse_args(['object', 'instance'])
        api_request = self.instantiate_apirequest(args)
        path = self.get_paths()['object']
        self.assertNotEqual(api_request.construct_path(), path)

    # test available fields flag
    def test_construct_path_w_available_fields(self):
        args = self.parser.parse_args(['instance', 'object', '--available_fields'])
        api_request = self.instantiate_apirequest(args)
        path = self.get_paths()['available_fields']
        self.assertEqual(api_request.construct_path(), path)

    # test get_report_columns flag
    def test_construct_path_w_get_report_columns(self):
        args = self.parser.parse_args(['instance', 'object', '--get_report_columns', 'report_name'])
        api_request = self.instantiate_apirequest(args)
        path = self.get_paths()['get_report_columns']
        self.assertEqual(api_request.construct_path(), path)

    # test get_report_inputs flag
    def test_construct_path_w_get_report_inputs(self):
        args = self.parser.parse_args(['instance', 'object', '--get_report_inputs', 'report_name'])
        api_request = self.instantiate_apirequest(args)
        path = self.get_paths()['get_report_inputs']
        self.assertEqual(api_request.construct_path(), path)

    # test get_reportlist flag
    def test_construct_path_w_get_reportlist(self):
        args = self.parser.parse_args(['instance', 'object', '--get_reportlist'])
        api_request = self.instantiate_apirequest(args)
        path = self.get_paths()['get_reportlist']
        self.assertEqual(api_request.construct_path(), path)

    # test id flag
    def test_construct_path_w_id(self):
        args = self.parser.parse_args(['instance', 'object', '--id', '123456789'])
        api_request = self.instantiate_apirequest(args)
        path = self.get_paths()['id']
        self.assertEqual(api_request.construct_path(), path)

    # test run flag
    def test_construct_path_w_run(self):
        args = self.parser.parse_args(['instance', 'object', '--run', 'report_name'])
        api_request = self.instantiate_apirequest(args)
        path = self.get_paths()['run']
        self.assertEqual(api_request.construct_path(), path)

    # test single path segment addition
    def test_construct_path_w_path_segment(self):
        args = self.parser.parse_args(['instance', 'object', '--path_segment', 'segment'])
        api_request = self.instantiate_apirequest(args)
        path = self.get_paths()['segment']
        self.assertEqual(api_request.construct_path(), path)

    # test multiple path segment additions
    def test_construct_path_w_path_segments(self):
        args = self.parser.parse_args(['instance', 'object', '--path_segment', 'segment1', 'segment2'])
        api_request = self.instantiate_apirequest(args)
        path = self.get_paths()['segments']
        self.assertEqual(api_request.construct_path(), path)

    # changing the order of additional path segments matters
    def test_construct_path_w_path_segments_out_of_order(self):
        args = self.parser.parse_args(['instance', 'object', '--path_segment', 'segment1', 'segment2'])
        api_request = self.instantiate_apirequest(args)
        path = self.get_paths()['segments_out_of_order']
        self.assertNotEqual(api_request.construct_path(), path)

    # test adding path segments to another flag (even if the result doesn't
    # make sense)
    def test_construct_path_w_available_fields_w_path_segments(self):
        args = self.parser.parse_args(['instance', 'object', '--available_fields', '--path_segment', 'segment1',
                                       'segment2'])
        api_request = self.instantiate_apirequest(args)
        path = self.get_paths()['available_fields_segments']
        self.assertEqual(api_request.construct_path(), path)

    # -----Test the construct_path() method - permutations-----

    # -----Test the construct_query_string() method - basic functionality-----

    # test method with no query parameters
    def test_construct_query_string_wo_flags(self):
        args = self.parser.parse_args(['instance', 'object'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('null')
        self.assertEqual(api_request.construct_query_string(), params)

    # =====Tests for list parameters=====
    # test method with limit parameter
    def test_construct_query_string_w_limit_100_wo_offset(self):
        args = self.parser.parse_args(['instance', 'object', '--limit', '100'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('l100')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with limit parameter = 0; defaults to an empty dict
    def test_construct_query_string_w_limit_0_wo_offset(self):
        args = self.parser.parse_args(['instance', 'object', '--limit', '0'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('null')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with limit parameter = 500; defaults to limit=10
    def test_construct_query_string_w_limit_500_wo_offset(self):
        args = self.parser.parse_args(['instance', 'object', '--limit', '500'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('l10')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with offset parameter
    def test_construct_query_string_wo_limit_w_offset_10(self):
        args = self.parser.parse_args(['instance', 'object', '--offset', '10'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('o10')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with limit and offset parameters
    def test_construct_query_string_w_limit100_w_offset_10(self):
        args = self.parser.parse_args(['instance', 'object', '--limit', '100', '--offset', '10'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('l100', 'o10')
        self.assertEqual(api_request.construct_query_string(), params)

    # =====Tests for report parameters=====
    """In Rick, when the --run flag is set, construct_query_string() will set 'report', 'start_date', 'end_date', and 
    'report_format' parameters regardless of whether these are explicitly set by the user, although 'report' is 
    slightly different in that it is set to the value of the required positional parameter to --run."""
    # test method with get_report_columns call
    def test_construct_query_string_w_get_report_columns(self):
        args = self.parser.parse_args(['instance', 'object', '--get_report_columns', 'report_type'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('get_report_columns')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with get_report_inputs call
    def test_construct_query_string_w_get_report_inputs(self):
        args = self.parser.parse_args(['instance', 'object', '--get_report_inputs', 'report_type'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('get_report_inputs')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with run flag (used for running reports)
    def test_construct_query_string_w_run(self):
        args = self.parser.parse_args(['instance', 'object', '--run', 'report_type'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('report', 'default_start_date', 'default_end_date', 'default_report_format')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with report format flag (defaults to json if not specified)
    def test_construct_query_string_w_report_format(self):
        args = self.parser.parse_args(['instance', 'object', '--run', 'report_type', '--format', 'format'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('report', 'default_start_date', 'default_end_date', 'report_format')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with relative start date and no end date
    def test_construct_query_string_w_run_w_rel_start_date(self):
        args = self.parser.parse_args(['instance', 'object', '--run', 'report_type', '--start_date', '2'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('report', 'relative_start_date', 'default_end_date', 'default_report_format')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with absolute start date and no end date
    def test_construct_query_string_w_run_w_abs_start_date(self):
        args = self.parser.parse_args(['instance', 'object', '--run', 'report_type',
                                       '--start_date', 'YYYY-mm-dd HH:MM:SS'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('report', 'absolute_start_date', 'default_end_date', 'default_report_format')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with relative end date and no start date
    def test_construct_query_string_w_rel_end_date(self):
        args = self.parser.parse_args(['instance', 'object', '--run', 'report_type', '--end_date', '2'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('report', 'default_start_date', 'relative_end_date', 'default_report_format')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with absolute end date and no start date
    def test_construct_query_string_w_abs_end_date(self):
        args = self.parser.parse_args(['instance', 'object', '--run', 'report_type',
                                       '--end_date', 'YYYY-mm-dd HH:MM:SS'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('report', 'default_start_date', 'absolute_end_date', 'default_report_format')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with relative start and end dates
    def test_construct_query_string_w_run_w_rel_start_date_w_rel_end_date(self):
        args = self.parser.parse_args(['instance', 'object', '--run', 'report_type', '--start_date', '2',
                                       '--end_date', '2'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('report', 'relative_start_date', 'relative_end_date', 'default_report_format')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with absolute start and end dates
    def test_construct_query_string_w_run_w_abs_start_date_w_abs_end_date(self):
        args = self.parser.parse_args(['instance', 'object', '--run', 'report_type',
                                       '--start_date', 'YYYY-mm-dd HH:MM:SS', '--end_date', 'YYYY-mm-dd HH:MM:SS'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('report', 'absolute_start_date', 'absolute_end_date', 'default_report_format')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with relative start date and absolute end date
    def test_construct_query_string_w_run_w_rel_start_date_w_abs_end_date(self):
        args = self.parser.parse_args(['instance', 'object', '--run', 'report_type', '--start_date', '2',
                                       '--end_date', 'YYYY-mm-dd HH:MM:SS'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('report', 'relative_start_date', 'absolute_end_date', 'default_report_format')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with absolute start date and relative end date
    def test_construct_query_string_w_run_w_abs_start_date_w_rel_end_date(self):
        args = self.parser.parse_args(['instance', 'object', '--run', 'report_type',
                                       '--start_date', 'YYYY-mm-dd HH:MM:SS', '--end_date', '2'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('report', 'absolute_start_date', 'relative_end_date', 'default_report_format')
        self.assertEqual(api_request.construct_query_string(), params)

    # =====Tests for other parameters=====
    # test method with one breakout
    def test_construct_query_string_w_breakout(self):
        args = self.parser.parse_args(['instance', 'object', '--breakouts', 'breakout'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('breakout')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with two breakouts
    def test_construct_query_string_w_breakouts(self):
        args = self.parser.parse_args(['instance', 'object', '--breakouts', 'breakout1,breakout2'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('breakouts')
        self.assertEqual(api_request.construct_query_string(), params)

    # test type_full call (used with available_fields)
    def test_construct_query_string_w_type_full(self):
        args = self.parser.parse_args(['instance', 'object', '--type_full', 'some_type'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('type_full')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with one additional query parameter
    def test_construct_query_string_w_query_param(self):
        args = self.parser.parse_args(['instance', 'object', '--query_params', 'param=test'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('param')
        self.assertEqual(api_request.construct_query_string(), params)

    # test method with two additional query parameters
    def test_construct_query_string_w_query_params(self):
        args = self.parser.parse_args(['instance', 'object', '--query_params', 'param1=test1', 'param2=test2'])
        api_request = self.instantiate_apirequest(args)
        params = self.get_params('params')
        self.assertEqual(api_request.construct_query_string(),
                         params)

    # -----Test the construct_query_string() method - permutations-----

    # -----Test combined paths and parameters-----


suite = unittest.TestLoader().loadTestsFromTestCase(RickTestCase)
unittest.TextTestRunner(verbosity=2).run(suite)
