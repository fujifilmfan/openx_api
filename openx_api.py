#!/usr/bin/env python

import argparse
from argparse import RawDescriptionHelpFormatter
import json
import os
import requests
import psycopg2

# import logging
# logging.basicConfig(level=logging.DEBUG)

import ox3apiclient
# from bunch import Bunch
import credentials as login_info
from customers import Customers

""""TO USE WITH CHARLES:
1. Uncomment 'import os' above
2. Uncomment the three proxy definitions below
3. Set "self.verify = False" in sessions.py; you might find it at:
/usr/local/lib/python2.7/site-packages/requests/sessions.py
"""

# charles_proxy = 'http://127.0.0.1:8888'
# os.environ['HTTP_PROXY'] = charles_proxy
# os.environ['HTTPS_PROXY'] = charles_proxy

# Global Constants
# for API requests:
API_PATH = '/ox/4.0'
API_PROTOCOL = 'http://'
# authentication constants:
ACCESS_PATH = '/api/index/token'
AUTHENTICATION_PROTOCOL = 'https://'
AUTHORIZATION_PATH = '/login/process'
REQUEST_PATH = '/api/index/initiate'
# for API database (enter your credentials in credentials.py):
API_DBNAME = login_info.credentials['api']['dbname']
API_HOSTDEVINT = login_info.credentials['api']['hostdevint']
API_HOSTPROD = login_info.credentials['api']['hostprod']
API_HOSTQA = login_info.credentials['api']['hostqa']
API_PASSWORD = login_info.credentials['api']['password']
API_USER = login_info.credentials['api']['user']
# for Unity (enter your credentials in credentials.py):
CORPORATE_INSTANCE = 'openxcorporate'
CORPORATE_EMAIL = login_info.credentials[CORPORATE_INSTANCE]['email']
CORPORATE_PASSWORD = login_info.credentials[CORPORATE_INSTANCE]['password']
ORIGIN_IP = '0.0.0.0'
TEST_CORPORATE_INSTANCE = 'test-corporate'
# credentials.py does not have a test-corporate entry by default; add a 'test-corporate' entry there and uncomment
# the following two lines to use test-corporate. Credentials are used in api_login().
TEST_CORPORATE_EMAIL = login_info.credentials[TEST_CORPORATE_INSTANCE]['email']
TEST_CORPORATE_PASSWORD = login_info.credentials[TEST_CORPORATE_INSTANCE]['password']


# Helper class to return CLI arguments
class ParseArgs(object):

    def __init__(self):
        pass

    @staticmethod
    def create_parser():
        description = 'Make API calls to OpenX.\n' \
            'The basic format of an API call is "protocol://api_hostname/path?parameters".\n' \
            'The api_hostname is fetched from customers.json using the instance short name passed as a required '\
            'argument on the command line.\n' \
            'An object comprises the third path segment in API calls (after /ox/4.0). Arguments that add segments to ' \
            'the path include:\n\t' \
            '--available_fields\n\t--get_report_columns\n\t--get_report_inputs\n\t--get_reportlist\n\t--id\n\t' \
            '--path-segment\n\t--run\n' \
            'Many calls can be modified with query parameters. Arguments that add parameters to the query string ' \
            'include:\n\t' \
            '--breakouts\n\t--end_date\n\t--format\n\t--limit\n\t--offset\n\t--query-param\n\t--start_date\n\t' \
            '--type_full\n' \
            'Access-related arguments (like Unity impersonation) include\n\t' \
            '--account\n\t--direct\n\t--impersonate\n\t--role (not currently used)\n' \
            'Lastly, arguments that affect output include:\n\t' \
            '--dry_run\n\t--json\n\t--name\n'

        parser = argparse.ArgumentParser(description=description,
                                         formatter_class=RawDescriptionHelpFormatter)

        parser.add_argument('instance', nargs='*', type=str, help="""Enter an
                            instance short name or ID. To access a QA instance,
                            an additional 'qa' flag is needed. Example usage:
                            \n$ rick.py rewardstyle
                            \n$ rick.py 1461 # ID for rewardstyle instance
                            \n$ rick.py 574 qa # ID for QA instance
                            """)
        parser.add_argument('object', type=str, help="""Enter a valid API
                            object or service. Available objects and services
                            include: account, ad, adunit, adunitgroup,
                            audiencesegment, comment, competitiveexclusion,
                            conversiontag, creative, deal, floorrule, lineitem,
                            optimization, order, package, paymenthistory,
                            report, site, sitesection, user.
                            See also https://docs.openx.com/Content/\
                            developers/platform_api/api_ref.html
                            """)
        parser.add_argument('-A', '--account', type=str, help="""Enter an
                            account ID for the account to which you need
                            access via the virtual user (work as self) Unity
                            feature. Use if you don't have backend access to
                            the API database or if you want to override the
                            default behavior of using the master network ID
                            of the instance.
                            """)
        parser.add_argument('-a', '--available_fields', action='store_true',
                            help="""Path modifier. Retrieve available fields
                            for the given object.
                            """)
        parser.add_argument('-b', '--breakouts', type=str, help="""Query
                            parameter. Optional when running a report.
                            Specify report breakouts with a comma-separated
                            list. Example usage:
                            \n--breakouts AdUnit,Country,CountryCode
                            """)
        parser.add_argument('-D', '--direct', type=str, help="""Use this flag
                            for direct instance access, in other words, to
                            bypass Unity. Requires a credential entry in
                            credentials.py. Example usage:
                            \n-D autobot
                            \n-D ebayuk_credentials
                            """)
        parser.add_argument('-d', '--dry_run', action='store_true', help="""
                            Use this flag to see the request URL and Unity
                            headers that would be used for a request without
                            actually making a request. This is not a
                            completely 'dry' dry run, as calls to
                            customers.json and the API database will still be
                            made to fill in needed parts of the URL and headers.
                            """)
        parser.add_argument('-e', '--end_date', type=str, help="""Query
                            parameter. Optional (for Rick) when running a
                            report. Enter either a relative date or a date in
                            ISO datetime format (YYYY-mm-dd HH:MM:SS). For
                            instance: 2017-01-01%%2023:59:59 or '2017-01-01
                            23:59:59'. If a date is not supplied, then '1' (
                            yesterday) is used as a default.
                            """)
        parser.add_argument('-f', '--format', type=str, default='json',
                            help="""Query parameter. Optional when running a
                            report. Specify the report_format. You can use
                            'csv' (no limitations), 'json' (maximum of 1000
                            rows), 'pdf' (no limitations), or 'xls' (maximum
                            of 65,000 rows). The default is json. See '--name'
                            for naming a downloaded report.
                            """)
        parser.add_argument('--get_report_columns', type=str, help="""Path
                            modifier and query parameter. Specify a report
                            code. Use this flag with the report object to
                            return the SRS report columns for the specified
                            report code.
                            """)
        parser.add_argument('--get_report_inputs', type=str, help="""Path
                            modifier and query parameter. Specify a report
                            code.  Use this flag with the report object to
                            return the SRS inputs for the specified report code.
                            """)
        parser.add_argument('--get_reportlist', action='store_true',
                            help="""Path modifier. Use this flag with the
                            report object to get a list of available reports
                            for your account.
                            """)
        parser.add_argument('--id', type=str, help="""Path modifier. Takes an
                            object ID and returns information about the object.
                            """)
        parser.add_argument('-I', '--impersonate', type=str, help="""Used with
                            Unity's 'impersonate' option. Enter the email
                            address of the user you want to impersonate.
                            """)
        parser.add_argument('-j', '--json', action='store_true',
                            help="""Converts Python dictionary to JSON in
                            printed output.
                            """)
        parser.add_argument('-l', '--limit', type=int, help="""Query
                            parameter. Enter the number of items to be
                            returned; sets the 'limit' parameter.  The API
                            defaults to 10, and the maximum allowed is 500.
                            """)
        parser.add_argument('-n', '--name', type=str, help="""Enter a name for
                            your report. You do not need to specify the
                            extension.
                            """)
        parser.add_argument('-o', '--offset', type=int, help="""Query
                            parameter. Enter an integer for the offset.  The
                            offset determines the nth object to return first.
                            The API defaults to 0. \nExample: limit=100&offset=0
                            will return the first 100 objects;
                            limit=100&offset=100 will return 100 objects
                            starting at the 101st object (I think);
                            limit=50&offset=100 will return objects 101-150;
                            limit=1&offset=99 will return the 100th object.
                            """)
        parser.add_argument('-p', '--path_segment', nargs='*', type=str,
                            help="""Path modifier. Add path segments in the
                            order that they should be added to the existing
                            path. The minimum existing path is /ox/4.0/object.
                            Example usage: \n--path_segment 538754068
                            """)
        parser.add_argument('-q', '--query_params', nargs='*', type=str,
                            help="""Query parameter(s). Add space-separated
                            query parameters. You might want to do this when
                            there is not a flag for the particular parameter
                            you want to add (or when the flag is unknown).
                            This can be used to override any report
                            parameters, whether those that are explicitly
                            specified or defaults). Example usage:
                            \n--query_params start_date=7 end_date=7
                            """)
        # 'Role' does not yet make sense within the Unity 'work as
        # self' environment.
        # parser.add_argument('-R', '--role', type=str, help="""Used with
        #                     Unity's 'work as self' option. Enter the user
        #                     role you would like to have.  The default value
        #                     of 'network.admin' will be used if you don't
        #                     specify a role.
        #                     """)
        parser.add_argument('-r', '--run', type=str, help="""Path modifier
                            and query parameter. Required to run a report.
                            Specify a report code, like 'inv_rev'.
                            """)
        parser.add_argument('-s', '--start_date', type=str, help="""Query
                            parameter. Optional (for Rick) when running a
                            report. Enter either a relative date or a date in
                            ISO datetime format (YYYY-mm-dd HH:MM:SS). For
                            instance: 2017-01-01%%2000:00:00 or '2017-01-01
                            00:00:00'. If a date is not supplied, then '1'
                            (yesterday) is used as a default.
                            """)
        parser.add_argument('-t', '--type_full', type=str, help="""Query
                            parameter. This is a field used with the
                            available_fields call for some objects.  Enter the
                            type of the object you are querying without
                            repeating the object name. For instance,
                            for 'adunit', you could enter 'videocompanion'.
                            """)

        return parser


# Main class for login, setting headers, constructing the request URL, making
# requests, printing results, and logout

class APIRequest(object):
    def __init__(self, cli_args):
        self.email = CORPORATE_EMAIL
        self.instance = CORPORATE_INSTANCE
        self.origin_ip = ORIGIN_IP
        self.password = CORPORATE_PASSWORD
        # api_hostname is the instance-specific hostname for API access
        self.api_hostname = None
        self.api_session = None
        self.base_url = None
        self.base_pickup_url = None
        self.cli_args = cli_args
        try:
            self.environment = 'qa' if cli_args.instance[1] == 'qa' else 'prod'
        except IndexError:
            self.environment = 'prod'
        self.customer_instance = cli_args.instance[0]
        self.request_url = None
        self.authenticated_request = requests.Session()

    # -----Handles authentication, Unity headers, and requests-----

    def api_login(self):
        """Authenticates via SSO and sets an access token in
        self.authenticated_request.cookies. Unity headers are set in
        self.authenticated_request.headers."""

        # Override default instance and credentials if cli_args.direct is set
        if self.cli_args.direct:
            self.email = login_info.credentials[self.cli_args.direct]['email']
            self.password = login_info.credentials[self.cli_args.direct]['password']

            # Establish API session to the user-specified instance
            self.api_session = self._environment_and_client_setup(self.customer_instance)
        # Use Unity to connect
        else:
            if self.environment == 'prod':
                self.email = CORPORATE_EMAIL
                self.password = CORPORATE_PASSWORD

                # Establish API session to openxcorporate
                self.api_session = self._environment_and_client_setup(CORPORATE_INSTANCE)
            elif self.environment == 'qa':
                self.email = TEST_CORPORATE_EMAIL
                self.password = TEST_CORPORATE_PASSWORD

                # Establish API session to test-corporate
                self.api_session = self._environment_and_client_setup(TEST_CORPORATE_INSTANCE)
            self._setup_headers()

        # Login to instance
        ox3apiclient.Client.logon(self.api_session, email=self.email, password=self.password)

        # Set access token for subsequent requests
        openx3_access_token = self.api_session.__dict__['_token']
        self.authenticated_request.cookies.set(name='openx3_access_token',
                                               value=openx3_access_token)
        return openx3_access_token

    def _environment_and_client_setup(self, instance):
        """This returns a session object from ox3apiclient to api_login.  By
        default, it fetches instance data for the openxcorporate instance since
        self.instance is set to CORPORATE_INSTANCE by default."""

        # The default value for prod and devint (if supported later) is xv.
        colo = 'ca' if self.environment == 'qa' else 'xv'

        instance_data = Customers(env=self.environment, colo=colo).get_customer_instance_by_name(instance)
        if instance_data is None:
            instance_data = Customers(env=self.environment, colo=colo).get_customer_instance(instance)
        self.api_hostname = instance_data.api_hostname
        self.base_url = '{protocol}{host_name}{path}'.format(
            protocol=API_PROTOCOL, host_name=instance_data.api_hostname, path=API_PATH)
        client = ox3apiclient.Client(instance_data.sso_domain,
                                     instance_data.sso_realm,
                                     instance_data.consumer_key,
                                     instance_data.consumer_secret,
                                     request_token_url='{protocol}{host_name}{path}'.format(
                                         protocol=AUTHENTICATION_PROTOCOL,
                                         host_name=instance_data.sso_domain,
                                         path=REQUEST_PATH),
                                     authorization_url='{protocol}{host_name}{path}'.format(
                                         protocol=AUTHENTICATION_PROTOCOL,
                                         host_name=instance_data.sso_domain,
                                         path=AUTHORIZATION_PATH),
                                     access_token_url='{protocol}{host_name}{path}'.format(
                                         protocol=AUTHENTICATION_PROTOCOL,
                                         host_name=instance_data.sso_domain,
                                         path=ACCESS_PATH),
                                     api_path=API_PATH,
                                     email=self.email,
                                     password=self.password)
        return client

    def _setup_headers(self):
        """ This sets the headers for two of the Unity features: work-as and
        virtual user.  In the Unity UI, 'impersonate' == 'work-as' and
        'work as self' == 'virtual user'.  This method is not required for
        direct instance login.

        Work-as (impersonate) header format:       work-as_email|platform_hash
        Work-as logic is contained in the 'if' statement below.
        Virtual user (work as self) header format: account_id|platform_hash|role
        Virtual user logic is contained in the 'else' statement below.

        The script will auto-complete account_id, platform_hash, and role (
        set to 'network.admin'), so only the work-as_email needs to be
        specified by the user, by the -I flag.
        """
        # This fetches instance data for the user-requested instance.
        # The default value for prod and devint (if supported later) is xv.
        colo = 'ca' if self.environment == 'qa' else 'xv'

        instance_data = Customers(env=self.environment, colo=colo).get_customer_instance_by_name(self.customer_instance)
        if instance_data is None:
            instance_data = Customers(env=self.environment, colo=colo).get_customer_instance(self.customer_instance)
        self.customer_instance = instance_data.name

        platform_hash = instance_data.platform_hash
        if self.cli_args.impersonate:
            self.authenticated_request.headers.setdefault(
                key='X-OPENX-ORIGIN-IP', default=self.origin_ip)
            self.authenticated_request.headers.setdefault(
                key='X-OPENX-WORK-AS', default='{email}|{platform_hash}'
                    .format(email=self.cli_args.impersonate, platform_hash=platform_hash))
        else:
            """ There is currently no option in the Unity UI to both 'work as
            self' and specify a particular role. The main problem from the
            back-end perspective is which account_id to choose since the
            master account ID will not work with publisher.admin, for instance.
            """
            role = 'network.admin'  # if cli_args.role is None else
            # cli_args.role

            # This checks whether the user has overridden the default
            # behavior of fetching the master network account ID via the API
            # database.
            if not self.cli_args.account:
                account_id = self.retrieve_master_account_id()
            else:
                account_id = self.cli_args.account

            self.authenticated_request.headers.setdefault(
                key='X-OPENX-ORIGIN-IP', default=self.origin_ip)
            self.authenticated_request.headers.setdefault(
                key='X-OPENX-WORK-AS', default='{account_id}|{platform_hash}|{role}'
                    .format(account_id=account_id, platform_hash=platform_hash, role=role))

    def retrieve_master_account_id(self):
        """Retrieves the master account ID from API database for use by
        _setup_headers in constructing the X-OPENX-WORK-AS header (
        specifically for the Virtual user (work as self) option)."""
        query_string = """
            SELECT id
            FROM account a
                INNER JOIN customer c ON a.instance_uid = c.platform_hash
            WHERE
                c.name = '{instance}'
                AND
                a.obj->>'account_id' IS NULL
            """.format(instance=self.customer_instance)
        cursor = None
        host = None
        if self.environment == 'prod':
            host = API_HOSTPROD
        elif self.environment == 'qa':
            host = API_HOSTQA
        try:
            connection = psycopg2.connect(dbname=API_DBNAME, user=API_USER, host=host, password=API_PASSWORD)
            cursor = connection.cursor()
        except psycopg2.OperationalError:
            print("Unable to connect to the API database. Check your connection settings.")
        try:
            cursor.execute(query_string)
        except psycopg2.ProgrammingError:
            print("Query execution failed.")
        rows = cursor.fetchall()
        master_account_id = rows[0][0]
        return master_account_id

    def api_get(self, path, params):
        self.request_url = '{base_url}/{path}'.format(base_url=self.base_url, path=path)
        response = self.authenticated_request.get(self.request_url, params=params)
        # the full URL is used by print_request_context()
        self.request_url = response.url
        return response.json()

    # Used for making dry runs and by Morty for testing; the value of p.url
    # should be the same as response.url from api_get() without actually
    # making the request
    def prepare_url(self, path, params):
        self.request_url = '{base_url}/{path}'.format(base_url=self.base_url, path=path)
        p = requests.Request('GET', self.request_url, params=params).prepare()
        self.request_url = p.url

    # -----The following section handles request URL construction-----

    def construct_path(self):
        path = [self.cli_args.object]
        path.append('available_fields') if self.cli_args.available_fields else ''
        path.append('get_report_columns') if self.cli_args.get_report_columns else ''
        path.append('get_report_inputs') if self.cli_args.get_report_inputs else ''
        path.append('get_reportlist') if self.cli_args.get_reportlist else ''
        path.append(self.cli_args.id) if self.cli_args.id else ''
        path.append('run') if self.cli_args.run else ''
        path.extend([segment for segment in self.cli_args.path_segment]) if \
            self.cli_args.path_segment else ''
        return '/'.join(path)

    def construct_query_string(self):
        params = {}
        if self.cli_args.get_report_columns:
            params['report'] = self.cli_args.get_report_columns
        if self.cli_args.get_report_inputs:
            params['report'] = self.cli_args.get_report_inputs
        if self.cli_args.run:
            params['report'] = self.cli_args.run
            params['start_date'] = self.cli_args.start_date if self.cli_args.start_date else '1'
            params['end_date'] = self.cli_args.end_date if self.cli_args.end_date else '1'
            params['report_format'] = self.cli_args.format if self.cli_args.format else 'json'
        if self.cli_args.breakouts:
            params['do_break'] = self.cli_args.breakouts
        if self.cli_args.limit:
            params['limit'] = self.cli_args.limit if 0 < self.cli_args.limit < 500 else 10
        if self.cli_args.offset:
            params['offset'] = self.cli_args.offset if self.cli_args.offset else 0
        if self.cli_args.type_full:
            params['type_full'] = '.'.join([self.cli_args.object,
                                            self.cli_args.type_full])
        if self.cli_args.query_params:
            for param in self.cli_args.query_params:
                key, value = param.split("=")
                params[key] = value
        return params

    @staticmethod
    def print_error(error_message):
        if error_message == '':
            print("No errors.")
        else:
            print(error_message)

    # -----The following section handles responses-----

    # Checks if a particular key exists in the request response (dictionary)
    # why is the response to /deal/536875919 (for ebayus) a list but not a list
    #  for /deal ?
    @staticmethod
    def key_exists(key, response):
        if type(response) is list:
            return False
        else:
            return bool(key in response.keys())

    def name_report(self):
        # refactor using .join
        if self.cli_args.run and self.cli_args.name:
            return '.'.join([self.cli_args.name, self.cli_args.format])
        else:
            return '.'.join([self.cli_args.run, self.cli_args.format])

    def retrieve_report(self, response):
        filename = self.name_report()
        print("Your report was downloaded to: {}/{}".format(os.getcwd(), filename))
        self.base_pickup_url = '{protocol}{host_name}'.format(protocol=API_PROTOCOL, host_name=self.api_hostname)
        pickup_url = self.base_pickup_url + response['url']
        print('The pickup URL is: {}'.format(pickup_url))
        r = requests.get(pickup_url)
        with open(filename, "wb") as report:
            report.write(r.content)

    def process_response(self, response):
        if self.cli_args.json:
            print("OX response: \n" + json.dumps(response, sort_keys=False, indent=4))
        else:
            print("OX response: \n" + str(response))
        # the 'url' key is used to designate the pickup URL
        if self.key_exists('url', response):
            self.retrieve_report(response)

    def print_request_context(self):
        print("-------------------\n  REQUEST CONTEXT  \n-------------------")
        print("Request URL:\n\t{}".format(self.request_url))
        try:
            origin_message = "Unity X-OPENX-ORIGIN-IP header:\n\t{}".format(self.origin_ip)
            work_as_message = "Unity X-OPENX-WORK-AS header:\n\temail|platform_hash\n\t{}" \
                .format(self.authenticated_request.headers['X-OPENX-WORK-AS'])
            virtual_user_message = "Unity X-OPENX-WORK-AS header:\n\taccount_id|platform_hash|role\n\t{}" \
                .format(self.authenticated_request.headers['X-OPENX-WORK-AS'])
            if self.cli_args.impersonate:
                print('\n'.join([origin_message, work_as_message]))
            else:
                print('\n'.join([origin_message, virtual_user_message]))
        except KeyError:
            print("Unity headers were not used for this request.")

    def api_logoff(self):
        """ Uses the ox3apiclient Client object to logoff instance.
        No arguments needed.  Nothing is returned.
        """
        ox3apiclient.Client.logoff(self.api_session)

# -----The following section runs the script-----


def make_api_request():
    # parse command-line arguments
    cli_args = ParseArgs().create_parser().parse_args()
    # instantiate class'urllog
    api_request = APIRequest(cli_args)
    # 1. create an ox3apiclient.Client() instance
    # 2. set up Unity headers
    # 3. login to OpenX
    # 4. open a requests.Session() with the openx3_access_token from step 1
    api_request.api_login()

    # construct user-defined path beyond /ox/4.0
    path = api_request.construct_path()
    # construct user-defined query string
    params = api_request.construct_query_string()

    # make API call and process response, or make a dry run
    if not cli_args.dry_run:
        # make API call
        response = api_request.api_get(path, params)
        # format response
        api_request.process_response(response)
    else:
        api_request.prepare_url(path, params)

    # print request context
    api_request.print_request_context()
    # logoff
    api_request.api_logoff()


if __name__ == '__main__':
    make_api_request()
