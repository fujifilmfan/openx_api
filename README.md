Using Rick for command line API requests
=====

Table of Contents
-----
* [Introduction](#introduction)  
* [Installation](#installation)  
* [Getting Started](#getting-started)  
* [Getting Help](#getting-help)  
* [Modes of Operation](#modes)  
  * [Accessing Instances Using Unity](#unity)  
  * [Accessing Instances Directly](#direct)  
* [Example Use](#example-use)  
  * [Available Fields](#available-fields)  
  * [Object](#object)  
  * [List](#list)  
  * [Report](#report)  
  * [Special Arguments](#special)  
* [Known Issues](#issues)  
  
<a name="introduction"></a>Introduction
-----
Rick is useful for making quick object and reporting queries using the OpenX API without having to login to the UI.  
It currently only makes GET requests.  
  
Here is a summary of the explicitly supported requests:  
* Available fields: used for making the /available_fields call on an object, like 
`/adunit/available_fields?type_full=adunit.web`
* Object: used for returning information about a particular object by specifying its ID; for example `/adunit/538379628`
* List: used for returning a list of objects such as `/adunit`
* Report: used for making the /report calls, including running reports; examples:
  * `/report/run?report=inv_rev&start_date=2017-01-01%2000:00:00&end_date=2017-01-01%2023:59:59&report_format=json`
  * `/report/get_reportlist`
  * `/report/get_report_inputs?report=inv_rev`
  * `/report/get_report_columns?report=inv_rev`
  
<a name="installation"></a>Installation
-----
(For macOS)  
1. Download and configure Rick  
    * Get Rick at <https://github.corp.openx.com/support-team/Rick>  
        * Use Git to clone the repository if you want.  
        * Otherwise, click the green "Clone or download" button and then "Download ZIP".  
        * Unzip the repository in a meaningful location on your machine.  
    * Add your credentials to credentials.py.  
2. Setup your machine for Python and virtualenv (I copied or modified most of Step 2 from 
[Definitive guide to python on Mac OSX](https://medium.com/@briantorresgil/definitive-guide-to-python-on-mac-osx-65acd8d969d0), 
but that guide is out of date at this writing with respect to installing python2 vs. python3 - `brew install python` 
will now install python3 by default)  
    * Install Xcode (it's required to install Homebrew and Python).  
        * Download it from the App Store.  
        * Open a terminal and type `xcode-select --install`.  
    * Install Homebrew  
        * Go to <https://brew.sh/> and follow the instructions there.  
        * At the time of this writing, the instruction is to enter this in a terminal:  
        `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`  
    * Install Python;  
        * Enter `brew install python@2` into a terminal. That will install Python 2, which Rick uses.  
        * If you also want Python 3 (preferred in general), enter `brew install python`.  
    * Install virtualenv and virtualenvwrapper  
        **Virtualenv** is short for "Virtual Environment". It allows you to easily work on Python applications and 
        obtain their required packages without affecting the global (operating system) Python installation.  
        **Virtualenvwrapper** is a tool that makes using virtualenv really easy.  
        * `pip install virtualenv`  
        * `pip install virtualenvwrapper`  
        * Upgrade pip: `pip install -U pip`  
    * Update your terminal environment  
        * Add the following lines to your `.bash_profile` file (at `~/.bash_profile`) using a command line editor like 
        nano, vi, emacs, etc.  
            ```bash
            # activate virtualenvwrapper
            source /usr/local/bin/virtualenvwrapper.sh
    
            # pip should only run if there is a virtualenv currently activated
            export PIP_REQUIRE_VIRTUALENV=true
    
            # create commands to override pip restriction.
            # use `gpip` or `gpip3` to force installation of
            # a package in the global python environment
            gpip(){
               PIP_REQUIRE_VIRTUALENV="" pip "$@"
            }
            gpip3(){
               PIP_REQUIRE_VIRTUALENV="" pip3 "$@"
            }
            ```
    
        * I recommend also adding an entry that points to your rick installation, something like this: 
        `export PATH="$PATH:$HOME/bin:."` (I keep my Rick at ~/bin/rick); this will allow you to use Rick from anywhere
        on your machine.  
        * Save the file and then close and re-open your terminal window to activate the changes; alternatively,  
        enter `source ~/.bash_profile`.  
3. Create a virtual environment for Rick  
    * `mkvirtualenv rick`  
    * You should see the name of your virtual environment in your command prompt like `(rick) computer1:~$`  
    * Exit the virtual environment using `deactivate`.  
    * Re-enter the virtual environment using `workon rick`  
4. Install dependencies  
    * While in the virtual environment, enter `pip install ox3apiclient requests psycopg2 psycopg2-binary bunch` 
    (these are requirements for Rick).   
  
<a name="getting-started"></a>Getting Started
-----
1. If Rick is not in your path, navigate to it's directory.  
2. Once there, you can run Rick like this:  
    * basic structure: $ rick.py *instance* *object* *optional_arguments*  
      * *instance* is the customer instance short name or ID (Rick first tries to find *instance* as a name in 
      customers.json, and if that fails, it looks for *instance* as an ID)
        * *instance* takes "qa" as an optional argument after the instance name or ID to set the environment to QA
        instead of Prod
      * *object* is the object or service type (such as adunit, 
  lineitem, deal, report, etc.); both *instance* and *object* are required, positional arguments
    * example: `$ rick.py ebayus report --run inv_rev --start_date 5 --end_date 5 -j`
3. Output is currently limited to Python dictionaries or JSON.  
4. Commands are expanded from something like  
    * $ rick.py *instance* *object* *optional_arguments* to  
    * http://*admin_hostname*/ox/4.0/*object*/*optional_arguments*
  
<a name="getting-help"></a>Getting Help
-----
The -h and --help flags can be used to get help on argument use.  For example:  
`$ rick.py -h` or `$ rick.py --help`  
  
<a name="modes"></a>Modes of Operation
-----
Arguments covered in this section:  
  * -D, --direct  
  * -I, --impersonate  
  
Rick has two basic modes of operation: direct instance access and access through Unity. Depending on your credentials, 
you can access either production (default) or QA instances.  
### <a name="unity"></a>Accessing Instances Using Unity  
#### Work As Self  
This is Rick's default mode, and all of the examples in the [Example Use](#example-use) section use this mode. To use
test Unity, add the `qa` argument to *instance*.  
  * prod basic structure: $ rick.py *instance* *object* *optional_arguments*
  * prod example: `$ rick.py ebayus lineitem --available_fields --type volume_goal --json`  
  * qa basic structure: $ rick.py *instance* qa *object* *optional_arguments*  
  * qa example: `$ rick.py 574 qa lineitem --available_fields --type volume_goal --json`  
#### Impersonation  
Add the `-I` or `--impersonate` flag with the email address of the user you want to impersonate.  To use test Unity, add the
`qa` argument to *instance*.  
  * prod basic structure: $ rick.py *instance* *object* --impersonate *email_address*
  * prod example: `$ rick.py rewardstyle lineitem --limit 1 --impersonate alishaself@rewardstyle.com --json`
  * qa basic structure: $ rick.py *instance* qa *object* --impersonate *email_address*
  * qa example: `$ rick.py 574 qa lineitem --limit 1 --impersonate oxbot+api_network_admin@openx.com --json`
### <a name="direct"></a>Accessing Instances Directly 
Add an entry to credentials.py to use this feature. Once you've added an entry with your direct-access
credentials, add the `-D` or `--direct` flag with the name of that entry as an argument. For QA instances, add the `qa`
argument to *instance*.
  * prod basic structure: $ rick.py *instance* *object* --direct *entry_in_credentials.py*  
  * prod example: `$ rick.py ebayus lineitem --limit 1 --direct ebayus_credentials --json`  
  * qa basic structure: $ rick.py *instance* qa *object* --direct *entry_in_credentials.py*  
  * qa example: `$ rick.py 574 qa lineitem --limit 1 --direct root --json`  
    
<a name="example-use"></a>Example Use
-----
### <a name="available-fields"></a>Available fields
Arguments covered in this section:  
  * -a, --available_fields  
  * -t, --type_full  
  
Use `--available_fields` to invoke an available fields call.  For objects with sub-types, use `-t` or `--type_full` 
to specify the particular type.  
  
A call involving an object that has only one type:  
`$ rick.py ebayus report --available_fields --json`  
results in the following URL:  
> /ox/4.0/report/available_fields  
  
A call involving an object that has sub-types:  
`$ rick.py ebayus lineitem --available_fields --json`  
results in the following URL:  
> /ox/4.0/lineitem/available_fields  
  
and gets the following response:  
```javascript
{
    "attribute": "type_full", 
    "value": null, 
    "choices": [
        "lineitem.exchange", 
        "lineitem.network", 
        "lineitem.exclusive", 
        "lineitem.house", 
        "lineitem.share_of_voice", 
        "lineitem.volume_goal", 
        "lineitem.non_guaranteed"
    ], 
    "field": {}, 
    "http_status": 400, 
    "message": "Field type_full value must be one of \"lineitem.exchange\", \"lineitem.network\", \"lineitem.exclusive\", \"lineitem.house\", \"lineitem.share_of_voice\", \"lineitem.volume_goal\" or \"lineitem.non_guaranteed\" (\"None\" not allowed)", 
    "type": "Value Error"
}
```
OpenX prompts the user to specify the type of object.  
  
A call in which the sub-type is specified:  
`$ rick.py ebayus lineitem --available_fields --type volume_goal --json`  
results in the following URL:  
> /ox/4.0/lineitem/available_fields?type_full=lineitem.volume_goal  
  
### <a name="object"></a>Object
Argument covered in this section:  
  * --id  
  
Use `--id` with an object ID to get information about a particular object.  
  
A call for a particular object:  
`$ rick.py ebayus adunit --id 538199076 --json`  
results in the following URL:  
> /ox/4.0/adunit/538199076  
  
### <a name="list"></a>List
Arguments covered in this section:  
  * -l, --limit  
  * -o, --offset  
  
No flags are needed to retrieve a list of objects.  Use `-l` or `--limit` to specify the maximum number of objects to retrieve in the list, and use `-o` or `--offset` to specify which object to return first.  
  
A call for a list of objects without specifying the number or offset (default of 10 objects with offset of 0):  
`$ rick.py ebayus order --json`  
results in the following URL:  
> /ox/4.0/order  
  
A call for a list of objects specifying one object without offset:  
`$ rick.py ebayus order --limit 1 --json`  
results in the following URL:  
> /ox/4.0/order?limit=1&offset=0  
  
A call for a list of objects specifying only the tenth object:  
`$ rick.py ebayus order --limit 1 --offset 9 --json`  
results in the following URL:  
> /ox/4.0/order?limit=1&offset=9  
  
You can also search for items by ID, name, and other fields. Use the `-q` or `--query_params` flag to specify additional
parameters. In addition to searching by different fields than ID, the list call supports filtering, sorting, and 
pagination.  
`$ rick.py ebayus adunit --query_params name=160x600_Homepage -j`  
results in the following URL:  
> /ox/4.0/adunit?name=160x600_Homepage  
  
### <a name="report"></a>Report
Arguments covered in this section:
  * -b, --breakouts    
  * -e, --end_date  
  * -f, --format  
  * --get_report_columns  
  * --get_report_inputs  
  * --get_reportlist  
  * -n, --name 
  * -r, --run  
  * -s, --start_date  
  
#### Getting report metadata  
To make calls for information about reports, use `--get_reportlist`, `--get_report_inputs`, or `--get_report_columns`.  
  
A call for a list of reports:  
`$ rick.py ebayus report --get_reportlist --json`  
results in the following URL:  
> /ox/4.0/report/get_reportlist  
  
A call for report inputs:  
`$ rick.py ebayus report --get_report_inputs inv_rev --json`  
results in the following URL:  
> /ox/4.0/report/get_report_inputs?report=inv_rev  
  
A call for report columns:  
`$ rick.py ebayus report --get_report_columns inv_rev --json`  
results in the following URL:  
> /ox/4.0/report/get_report_columns?report=inv_rev  
  
#### Running reports  
There are three fields that are required to run a report:  
* report code  
* start date  
* end date  
  
The report code is passed on the command line as a positional argument to the `-r` or `--run` flag.  Start date and  
end date can be passed via the `-s` or `--start_date` and `-e` or `--end_date`, respectively, but Rick will use a  
value of '1' (yesterday) if a date is not specified.  
  
The default report format is JSON. Use the `-f` or `--format` flag to specify a different format. You can use 'csv',  
'json', 'pdf', or 'xls'. Rick will only download reports if there is a 'url' field in the response, and it will  
download them automatically to your current working directory. The 'url' field specifies the path to the report on  
OpenX servers, and it is not returned for JSON reports. By default, Rick will name downloaded reports as  
report_code + file_extension. You can change the default naming convention by using the `-n` or `--name` flag and then  
specifying the name. Since file extension is added by Rick based on the report format, you don't need to add it.  
  
A call for a report with only the report code specified:  
`$ rick.py ebayus report --run inv_rev --json`  
results in the following URL:  
> /ox/4.0/report/run?report=inv_rev&start_date=1&end_date=1&report_format=json  
  
A call for a report with relative dates:  
`$ rick.py ebayus report --run inv_rev --start_date 1 --end_date 1 --json`  
results in the following URL:  
> /ox/4.0/report/run?report=inv_rev&start_date=1&end_date=1&report_format=json  
  
A call for a report with absolute dates:  
`$ rick.py ebayus report --run inv_rev --start_date '2016-07-11 00:00:00' --end_date '2016-07-11 23:59:59' --json`  
results in the following URL:  
> /ox/4.0/report/run?report=inv_rev&start_date=2016-07-11 00:00:00&end_date=2016-07-11 23:59:59&report_format=json  
  
To add breakouts to the report, use the `-b` or `--breakouts` flag with a comma-separated list of breakout options.
`$ rick.py ebayus report --run exch_perf --breakouts Country,CountryCode -j`  
results in the following URL:  
> /ox/4.0/report/run?report=exch_perf&do_break=Country%2CCountryCode&start_date=1&end_date=1&report_format=json  

Changing report format and name:  
`$ rick.py ebayus report --run exch_perf --format csv --name ebay_exch_perf_report -j`  
results in the following URL:  
> /ox/4.0/report/run?report=exch_perf&do_break=Country%2CCountryCode&start_date=1&end_date=1&report_format=csv  
  
### <a name="special"></a>Special Arguments  
Arguments covered in this section:  
  * -A, --account  
  * -d, --dry_run  
  * -p, --path_segment  
  * -q, --query_params  
  
Use `-A` or `--account` if you do not have direct access to JSONdb (the API database).  In that case, use this flag 
with the master network ID for the instance you want to access. You can run the following query at 
<https://api-jsondb.xv.dc.openx.org/production/> to get the master network ID (replace INSTANCE_SHORT_NAME with the 
name, like 'ebayus' or 'yahoo-bidout').
```
SELECT a.id, jsonb_pretty(a.obj)
FROM account a
  INNER JOIN customer c ON a.instance_uid = c.platform_hash
WHERE c.name = 'INSTANCE_SHORT_NAME' and a.obj->>'account_id' IS NULL
```
  
Use `-d` or `--dry_run` to see the URL and Unity request headers that would be generated by the script without  
actually making an API call.  
  
The path segment flag (`-p` or `--path_segment`) is a powerful flag that allows you to add '/'-separated path segments  
that aren't currently explicitly supported by Rick.  
  
Similarly, the query parameters flag (`q` or `--query_params`) extends Rick to allow an arbitrary number of query  
parameters to be added to an API call. There is an example of usage in the [List](#list) section.  
  
<a name="issues"></a>Known Issues  
-----
  